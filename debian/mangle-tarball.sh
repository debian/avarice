#!/bin/bash

# from the svn we get an extra "avarice/" directory in the tarball.
# this hack removes it.
# eg avarice-2.14+svn392/avarice/ should become avarice-2.14+svn392

set -e

TARNAME="avarice_$2.orig.tar.xz"
NEEDLE="avarice-$2/avarice"

if tar tf "../$TARNAME" "$NEEDLE/" >/dev/null ; then
   echo "Removing extra avarice directory from tarball."
   TMPDIR=$(mktemp -d)
   tar xf "../$TARNAME" -C "$TMPDIR"
   tar cJf "../new_$TARNAME" --transform "s#$NEEDLE#avarice-$2#" --show-transformed-names -C "$TMPDIR" .
   TARGET=$(readlink -f "../$TARNAME")
   mv "../new_$TARNAME" "$TARGET"
fi

rm -rf "$TMPDIR"

