Source: avarice
Section: electronics
Priority: optional
Maintainer: Tobias Frost <tobi@debian.org>
Build-Depends: autoconf-archive,
               binutils-dev,
               debhelper-compat (= 13),
               libhidapi-dev [!hurd-any],
               libiberty-dev,
               libusb-dev,
               zlib1g-dev
Build-Conflicts: binutils-multiarch
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: http://avarice.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/avarice.git
Vcs-Browser: https://salsa.debian.org/debian/avarice

Package: avarice
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: gdb-avr
Description: use GDB with Atmel AVR debuggers
 AVaRICE is a program which interfaces the GNU Debugger with the AVR JTAG ICE,
 and other debuggers, available from Atmel. It connects to gdb via a TCP socket
 and communicates via gdb's "serial debug protocol".
 .
 This protocol allows gdb to send commands like "set/remove breakpoint" and
 "read/write memory". AVaRICE translates this commands into the Atmel protocol
 used to control the JTAG ICE (or other) debugger.
 .
 Because the GDB-AVaRICE connection is via a TCP socket, the two programs do
 not need to run on the same machine.
 .
 The currently supported debuggers are:
 .
  * JTAG ICE mkI
  * JTAG ICE mkII
  * AVR Dragon
